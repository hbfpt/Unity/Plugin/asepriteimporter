# AsepriteImporter

将Aseprite文件直接导入进Unity使用

## 如何使用

1. 复制git地址：`https://gitlab.com/hbfpt/Unity/Plugin/asepriteimporter.git`

2. 打开工程，点击窗口->包管理器->加号->添加来自git URL的包

![image.png](https://s2.loli.net/2023/04/08/b1rn9XVG6diJq7v.png)

3. 输入复制的git地址
4. 等待下载完成即可！直接将 `.seprite`或 `.ase`导入进工程看看？

（支持导入帧并生成动画，支持调整一些参数）
